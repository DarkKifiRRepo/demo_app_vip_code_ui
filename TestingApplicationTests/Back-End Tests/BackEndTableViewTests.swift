//
//  BackEndTableViewTests.swift
//  TestingApplicationTests
//
//  Created by Александр Евсеев on 15/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import XCTest
@testable import TestingApplication

class BackEndTableViewTests: XCTestCase {

    var sut: CustomTableView!
    var viewController: MockViewController!
    
    override func setUp() {
        super.setUp()
        viewController = MockViewController()
        sut = CustomTableView()
        viewController.table = sut
        
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        sut = nil
        viewController = nil
        super.tearDown()
    }

    func testInitTableView() {
        XCTAssertNotNil(sut)
    }
    
    func addDataToTableView() {
        let good = GoodsViewModel.Cell.init(name: "foo", price: "100", count: "100")
        let goodsModel = GoodsViewModel.init(cells: [good])
        
        sut.refreshData(data: goodsModel)
    }
    
    func testNumberOfRowsDependsOnDataArray() {
        addDataToTableView()
        
        XCTAssertEqual(sut.numberOfRows(inSection: 0), 1)
    }
    
    func testTableViewCellIsCustom() {
        addDataToTableView()
        
        let cell = sut.cellForRow(at: IndexPath(row: 0, section: 0))
        
        XCTAssertTrue(cell is CustomTableViewCell)
    }
    
    func testCellConfigures() {
        addDataToTableView()
        
        let cell = sut.cellForRow(at: IndexPath(row: 0, section: 0)) as? CustomTableViewCell
        
        XCTAssertEqual(cell?.nameLabel.text, "foo")
    }
    
    func testSelectCellInTableViewSendNotification() {
        addDataToTableView()
        let good = GoodsViewModel.Cell.init(name: "foo", price: "100", count: "100")
        let dataToPass = (info: good, position: 0)
        
        expectation(forNotification: NSNotification.Name(rawValue: "didSelectRowAt"), object: nil) { (notification) -> Bool in
            guard let dataFromNorification = notification.userInfo?["cellData"] as? (GoodViewModel, Int) else { return false }
            return dataToPass.info.name == dataFromNorification.0.name
        }
        
        sut.delegate?.tableView?(sut, didSelectRowAt: IndexPath(row: 0, section: 0))
        waitForExpectations(timeout: 1, handler: nil)
    }
}

extension BackEndTableViewTests {
    class MockViewController: UIViewController {
        
        var table: UITableView!
        
        override func viewDidLoad() {
            table.fillSuperview()
        }
    }
}
