//
//  BackEndEditViewControllerTests.swift
//  TestingApplicationTests
//
//  Created by Александр Евсеев on 15/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import XCTest
@testable import TestingApplication

class BackEndEditViewControllerTests: XCTestCase {
    
    var sut: BackEndEditViewController!
    
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard.init(name: "BackEndFlow", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: BackEndEditViewController.self)) as? BackEndEditViewController
        sut = vc
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    
    func testVCInitWorkCurrectly() {
        _ = sut.view
        
        XCTAssertNotNil(sut.countStepper)
    }
    
    func testViewDidLoadSetValues() {
        
        sut.beginAppearanceTransition(true, animated: true)
        sut.endAppearanceTransition()
        
        XCTAssertEqual(sut.nameTextLabel.text, "Dummy")
    }
    
    func testOnViewDidAppearSetsValues() {
        let good = GoodsViewModel.Cell.init(name: "foo", price: "100", count: "100") as GoodViewModel
        let dataToPass: (GoodViewModel, Int?) = (info: good, position: 0)
        
        sut.dataForEdit = dataToPass
        sut.beginAppearanceTransition(true, animated: true)
        sut.endAppearanceTransition()
        
        XCTAssertEqual(sut.nameTextLabel.text, good.name)
    }
    
    func testSaveMethod() {
        sut.beginAppearanceTransition(true, animated: true)
        sut.endAppearanceTransition()
        let mockWorker = MockBackEndWorker()
        
        sut.setWorker( mockWorker )
        sut.saveNewModel()
        
        XCTAssertEqual(mockWorker.dataToPass?.0.name , "Dummy")
    }
}

extension BackEndEditViewControllerTests {
    class MockBackEndWorker: BackEndWorker {
        
        var dataToPass: (GoodViewModel, Int?)?
        
        override func updateObject(info: GoodViewModel, position: Int?) {
            dataToPass = (info, position)
        }
    }
}
