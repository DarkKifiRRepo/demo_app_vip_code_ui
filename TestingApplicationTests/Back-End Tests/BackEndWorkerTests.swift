//
//  BackEndWorker.swift
//  TestingApplicationTests
//
//  Created by Александр Евсеев on 15/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import XCTest
@testable import TestingApplication

class BackEndWorkerTests: XCTestCase {

    var sut: BackEndWorker!
    
    override func setUp() {
        super.setUp()
        
        sut = BackEndWorker()
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    func testInit() {
        XCTAssertNotNil(sut)
    }
    
    func testUpdateRequestSendData() {
        let tabBarController = TabBarController()
        let mockDataStore = MockDataStore()
        tabBarController.dataStore = mockDataStore
        
        let newSut = BackEndWorker(coordinator: tabBarController)
        let good = GoodsViewModel.Cell.init(name: "foo", price: "100", count: "100")
        
        newSut.updateObject(info: good, position: 0)
        
        XCTAssertNotNil(mockDataStore.dataToPass?.0)
        
        newSut.updateObject(info: good, position: nil)
        
        XCTAssertNotNil(mockDataStore.dataToPass?.0)
        XCTAssertNil(mockDataStore.dataToPass?.1)
    }
    
}

extension BackEndWorkerTests {
    
    class MockDataStore: CoordinatorDataStore {
        var dataToPass: (DataModel, Int?)?
        
        override func editGood(info: (data: DataModel, position: Int?)) {
            dataToPass = info
        }
    }
}
