//
//  BackEndViewControllerTests.swift
//  TestingApplicationTests
//
//  Created by Александр Евсеев on 15/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import XCTest
@testable import TestingApplication

class BackEndViewControllerTests: XCTestCase {

    var sut: BackEndViewController!
    
    override func setUp() {
        super.setUp()
        sut = UIStoryboard.init(name: "BackEndFlow", bundle: nil).instantiateInitialViewController() as? BackEndViewController
    }

    override func tearDown() {
        sut = nil
        
        super.tearDown()
    }

    func testInit() {
        XCTAssertNotNil(sut)
    }
    
    func setMockTable() -> MockTableView {
        let mockTableView = MockTableView()
        
        sut.setTableView(mockTableView)
        
        sut.beginAppearanceTransition(true, animated: true)
        sut.endAppearanceTransition()
        
        return mockTableView
    }
    
    func testViewIsLoadedCurrectly() {
        let mockTableView = setMockTable()
        
        XCTAssertNotNil(mockTableView)
    }
    
    func testDataSetupInTableView() {
        let mockTableView = setMockTable()
        sut.setTableView(mockTableView)
        
        let dataToSetup = GoodsViewModel.init(cells: [GoodsViewModel.Cell.init(name: "foo", price: "100", count: "10")])
        
        sut.refreshData(data: dataToSetup)
        let countOfRows = mockTableView.numberOfRows(inSection: 0)
        
        XCTAssertEqual(countOfRows, 1)
    }
    
    func testDidSelectRowAtWork() {
        let mockTableView = setMockTable()
        sut.setTableView(mockTableView)
        let dataToSetup = GoodsViewModel.init(cells: [GoodsViewModel.Cell.init(name: "foo", price: "100", count: "10")])
        sut.refreshData(data: dataToSetup)
        
        expectation(forNotification: NSNotification.Name("didSelectRowAt"), object: nil, handler: nil)
        
        mockTableView.delegate?.tableView?(mockTableView, didSelectRowAt: IndexPath(row: 0, section: 0))
        
        waitForExpectations(timeout: 0.2, handler: nil)
    }
    
    func testNotificationCallNewDataRefreshMethod() {
        let mockTableView = setMockTable()
        let response = StructDataModel.init(data: [DataModel.init(nameOf: "Foo", priceOf: 10, countOf: 10)])
        
        NotificationCenter.default.post(name: NSNotification.Name("NewData Arrieved"),
                                        object: nil,
                                        userInfo: ["NewData" : response])
        
        let result = mockTableView.checkData
        XCTAssertNotNil(result)
    }
}

extension BackEndViewControllerTests {
    class MockTableView: CustomTableView {
        var checkData: [GoodsViewModel.Cell] = []
        
        override func refreshData(data: GoodsViewModel) {
            checkData = data.cells
            super.refreshData(data: data)
        }
    }
}
