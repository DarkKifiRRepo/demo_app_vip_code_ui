//
//  BackEndTableViewCellTests.swift
//  TestingApplicationTests
//
//  Created by Александр Евсеев on 15/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import XCTest
@testable import TestingApplication

class BackEndTableViewCellTests: XCTestCase {

    var sut: CustomTableViewCell!
    
    override func setUp() {
        super.setUp()
        
        let table = CustomTableView()
        let good = GoodsViewModel.Cell.init(name: "foo", price: "100", count: "100")
        let goodsModel = GoodsViewModel.init(cells: [good])
        table.refreshData(data: goodsModel)
        
        sut = table.tableView(table, cellForRowAt: IndexPath(row: 0, section: 0)) as? CustomTableViewCell
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    func testCellHaveNameLabelAndCountLabel() {
        XCTAssertEqual(sut.nameLabel.text, "foo")
        XCTAssertEqual(sut.countLabel.text, "100 шт.")
    }
    
    func testPrepareForReuseMethodClearContent() {
        sut.prepareForReuse()
        
        XCTAssertEqual(sut.nameLabel.text, "")
    }
}
