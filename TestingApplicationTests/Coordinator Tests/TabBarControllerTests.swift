//
//  TabBarControllerTests.swift
//  TestingApplicationTests
//
//  Created by Александр Евсеев on 15/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import XCTest
@testable import TestingApplication

class TabBarControllerTests: XCTestCase {
    
    var sut: TabBarController!
    
    override func setUp() {
        super.setUp()
        let dataStore = CoordinatorDataStore(dataManager: nil)
        sut = TabBarController(dataStore: dataStore)
    }

    override func tearDown() {
        super.tearDown()
    }
    
    func testInitCurrently() {
        XCTAssertNotNil(sut)
    }
    
    func testViewControllersHaveTwoValues() {
        XCTAssertEqual(sut.viewControllers?.count, 2)
    }
    
    func testRequestCallGetRequestMethod() {
        let dModel = DataModel.init(nameOf: "Foo", priceOf: 10, countOf: 10)
        let request = CoordinatorRequestModel.Request.editGoodAt(position: nil, good: dModel)
        
        sut.getRequest(request: request)
        expectation(forNotification: NSNotification.Name("Coordinator Have New Data"), object: nil, handler: nil)
        
        waitForExpectations(timeout: 5.2) { (_) in
            let result = self.sut.dataStore?.store?.data.last
            XCTAssertEqual(result?.name, dModel.name)
        }
    }
    
    func testNavigationDelegateInvokeNewVC() {
        let _ = UINavigationController(rootViewController: sut)
        sut.presentEditViewController(DataToPass: nil)
        
        RunLoop.main.run(until: Date.init(timeIntervalSinceNow: 0.1)) // wait for animation
        
        let editVC = sut.navigationController?.topViewController as? BackEndEditViewController
        
        XCTAssertNotNil(editVC)
    }
}

