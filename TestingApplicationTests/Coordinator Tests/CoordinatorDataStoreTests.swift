//
//  CoordinatorDataStoreTests.swift
//  TestingApplicationTests
//
//  Created by Александр Евсеев on 15/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import XCTest
@testable import TestingApplication

class CoordinatorDataStoreTests: XCTestCase {

    var sut: CoordinatorDataStore!
    
    override func setUp() {
        super.setUp()
        
        sut = CoordinatorDataStore()
    }

    override func tearDown() {
        super.tearDown()
    }
    
    func fillDataInSut() {
        sut.dataManager = nil
        sut.store = StructDataModel.init(data: [DataModel.init(nameOf: "Foo", priceOf: 10, countOf: 10)])
    }
    
    func testGetAllDataReturnData() {
        fillDataInSut()
        var result: StructDataModel?
        let exp = expectation(description: "handler")
        
        sut.getDataFromStore { (handlerData) in
            result = handlerData
            exp.fulfill()
        }
        
        waitForExpectations(timeout: 0.2) { (_) in
            XCTAssertEqual(result?.data.last?.name, "Foo")
        }
        
    }
    
    func testBuyGoodEditValues() {
        let dModel = DataModel.init(nameOf: "Foo", priceOf: 10, countOf: 10)
        fillDataInSut()

        sut.buyGood(info: dModel)
        RunLoop.main.run(until: Date.init(timeIntervalSinceNow: 9))
        let result = sut.store?.data.first

        XCTAssertEqual(result?.count, 9)
    }
    
    func testEditGoodAddValues() {
        let dModel = DataModel.init(nameOf: "Bar", priceOf: 10, countOf: 10)
        fillDataInSut()
        
        sut.editGood(info: (data: dModel, position: nil))
        RunLoop.main.run(until: Date.init(timeIntervalSinceNow: 5.1))
        
        let result = sut.store?.data.last
        XCTAssertEqual(dModel.name, result?.name)
    }
    
    func testEditGoodChangeValue() {
        let dModel = DataModel.init(nameOf: "Bar", priceOf: 10, countOf: 10)
        fillDataInSut()
        
        sut.editGood(info: (data: dModel, position: 0))
        RunLoop.main.run(until: Date.init(timeIntervalSinceNow: 5.1))
        let result = sut.store?.data.first

        XCTAssertEqual(result?.name, dModel.name)
        
    }
}

extension CoordinatorDataStoreTests {
    class DataManagerSpy: DataManager {
        var fetchCalled = false
        
        override func fetchAndReadyData(completion: @escaping (StructDataModel) -> Void) {
            fetchCalled = true
        }
    }
}
