//
//  AdaptersTests.swift
//  TestingApplicationTests
//
//  Created by Александр Евсеев on 20/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import XCTest
@testable import TestingApplication

class AdaptersTests: XCTestCase {

    func testMethodModelToViewModel() {
        let testModel = StructDataModel.init(data: [DataModel.init(nameOf: "Foo", priceOf: 10, countOf: 10)])
        
        let result = Adapters.modelToViewModel(model: testModel)
        
        XCTAssertEqual(result.cells.first?.price, "10.0")
    }
}
