//
//  ViewControllerTests.swift
//  TestingApplicationTests
//
//  Created by Александр Евсеев on 15/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import XCTest
@testable import TestingApplication

class ViewControllerTests: XCTestCase {
    
    var sut: StoreFrontViewController!
    
    override func setUp() {
        super.setUp()
        let vc = StoreFrontViewController(transitionStyle: .scroll, navigationOrientation: .horizontal)
        sut = vc
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    func testInit() {
        XCTAssertNotNil(sut)
    }
    
    func testViewReactOnInitialFetchCase() {
        let viewModel = viewModelVC.init(viewsArray: [GalleryViewController.init()])
        let request = DataTransfer.Display.ViewModel.displayInitData(viewModel: viewModel)
        sut.displayData(viewModel: request)
        XCTAssertEqual(sut.viewControllers?.count, 1)
    }
    
    func testViewReactOnButtonPressedCase() {
        let viewModel = viewModelVC.init(viewsArray: [GalleryViewController.init()])
        let request = DataTransfer.Display.ViewModel.displayBuyAction(viewModel: viewModel)
        sut.displayData(viewModel: request)
        XCTAssertEqual(sut.viewControllers?.count, 1)
    }
    
    func testButtonPressedDelegate() {
        let spy = StoreFrontBusinessLogicSpy()
        sut.interactor = spy
        let model = GoodsViewModel.Cell.init()
        sut.buttonPressed(userInfo: model)
        
        XCTAssertTrue(spy.getRequestCalled)
    }
}

extension ViewControllerTests {
    class StoreFrontBusinessLogicSpy: BusinessLogic {
        var worker: StoreFrontWorker?
        var getRequestCalled = false
        
        func getRequest(request: DataTransfer.Request.RequestType) {
            getRequestCalled = true
        }
    }
}
