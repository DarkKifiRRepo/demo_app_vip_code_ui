//
//  PresenterTests.swift
//  TestingApplicationTests
//
//  Created by Александр Евсеев on 15/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import XCTest
@testable import TestingApplication

class PresenterTests: XCTestCase {

    var sut: StoreFrontPresenter!
    
    override func setUp() {
        super.setUp()
        
        sut = StoreFrontPresenter()
    }

    override func tearDown() {
        super.tearDown()
    }
    
    func testDisplayInit() {
        let spy = DisplayLogicSpy()
        sut.viewController = spy
        let data = StructDataModel.init(data: [DataModel.init(nameOf: "Foo", priceOf: 10, countOf: 10)])
        
        let response = DataTransfer.Present.PresentType.presentInitFetch(data: data)
        sut.presentData(response: response)
        
        XCTAssertTrue(spy.displayCalled)
    }
    
    func testDisplayBuyButton() {
        let spy = DisplayLogicSpy()
        sut.viewController = spy
        let data = StructDataModel.init(data: [DataModel.init(nameOf: "Foo", priceOf: 10, countOf: 10)])
        
        let response = DataTransfer.Present.PresentType.presentBuyAction(data: data)
        sut.presentData(response: response)
        
        XCTAssertTrue(spy.displayCalled)
    }
}

extension PresenterTests {
    class DisplayLogicSpy: DisplayLogic {
        var displayCalled = false
        
        func displayData(viewModel: DataTransfer.Display.ViewModel) {
            displayCalled = true
        }
    }
}
