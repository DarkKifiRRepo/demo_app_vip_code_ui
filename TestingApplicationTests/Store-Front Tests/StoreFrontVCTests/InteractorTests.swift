//
//  InteractorTests.swift
//  TestingApplicationTests
//
//  Created by Александр Евсеев on 15/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import XCTest
@testable import TestingApplication

class InteractorTests: XCTestCase {

    var sut: StoreFrontInterator!
    
    override func setUp() {
        super.setUp()
        
        sut = StoreFrontInterator()
        let worker = DummyWorker(coordinator: nil)
        sut.worker = worker
    }

    override func tearDown() {
        super.tearDown()
    }
    
    func testRequestGiveAll() {
        let spy = PresentableSpy()
        sut.presenter = spy

        let request = DataTransfer.Request.RequestType.updatePresentData
        sut.getRequest(request: request)

        XCTAssertTrue(spy.presentCalled)
    }
    
    func testRequestBuyButtonPressed() {
        let spy = PresentableSpy()
        sut.presenter = spy
        let data = GoodsViewModel.Cell.init(name: "Foo", price: "Bar", count: "Baz") as GoodViewModel

        let request = DataTransfer.Request.RequestType.buyButtonPressed(userInfo: data)
        sut.getRequest(request: request)

        XCTAssertTrue(spy.presentCalled)
    }
}

extension InteractorTests {
    class PresentableSpy: Presentable {
        var presentCalled = false
        
        func presentData(response: DataTransfer.Present.PresentType) {
            presentCalled = true
        }
    }
    
    class DummyWorker: StoreFrontWorker {
        override func buyGood(userInfo: GoodViewModel, completion: @escaping (StructDataModel) -> Void) {
            let data = StructDataModel.init(data: [])
            completion(data)
        }
        override func takeData(completion: @escaping (StructDataModel) -> Void) {
            let data = StructDataModel.init(data: [])
            completion(data)
        }
    }
}
