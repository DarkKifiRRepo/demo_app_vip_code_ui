//
//  GalleryViewControllerTests.swift
//  TestingApplicationTests
//
//  Created by Александр Евсеев on 15/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import XCTest
@testable import TestingApplication

class GalleryViewControllerTests: XCTestCase {

    var sut: GalleryViewController?
    
    override func setUp() {
        super.setUp()
        
        sut = GalleryViewController()
        _ = sut?.view
    }

    override func tearDown() {
        sut = nil
        
        super.tearDown()
    }

    func testGalleryVCsetupSubViews() {
        XCTAssertNotNil(sut?.countViewSimpleLabel)
        XCTAssertNotNil(sut?.buyButton)
    }
    
    func reInitSUTAndReturnDummy() -> MockViewController {
        let dummyVM = GoodsViewModel.Cell.init(name: "Foo", price: "100", count: "100")
        let mockVC = MockViewController()
        sut = GalleryViewController(with: dummyVM, delegate: mockVC, tag: 1)
        return mockVC
    }
    
    func testGalleryVCConvenienceInitWorkCurrently() {
        _ = reInitSUTAndReturnDummy()
        
        XCTAssertEqual(sut?.nameViewLabel.text, "Foo")
    }
    
    func testGalleryPressButtonWorkCurrently() {
        let mockVC = reInitSUTAndReturnDummy()
        
        sut?.buyButtonPressed()
        
        XCTAssertTrue(mockVC.methodFlag)
    }
    
    func testGallerySetDataAndSetValues() {
        let dummyVM = GoodsViewModel.Cell.init(name: "Foo", price: "100", count: "100")
        
        sut?.setData(with: dummyVM)
        
        XCTAssertEqual(sut?.countViewCountLabel.text, dummyVM.count! + " шт.")
        XCTAssertEqual(sut?.priceViewPriceLabel.text, dummyVM.price! + " руб.")
        XCTAssertEqual(sut?.nameViewLabel.text, dummyVM.name)
    }
    
    func testGalleryUpdateDataUpdateValues() {
        let dummyVM = GoodsViewModel.Cell.init(name: "Foo", price: "100", count: "100")
        
        sut?.updateViewModel(with: dummyVM)
        
        XCTAssertEqual(sut?.nameViewLabel.text, "Foo")
    }
}

extension GalleryViewControllerTests {
    class MockViewController: UIViewController, ButtonPressDelegate, DisplayLogic {
        func displayData(viewModel: DataTransfer.Display.ViewModel) {}
        
        var methodFlag = false
        
        func buttonPressed(userInfo: GoodViewModel) {
            methodFlag = true
        }
    }
}
