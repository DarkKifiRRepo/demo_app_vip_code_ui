//
//  DataManagerTests.swift
//  TestingApplicationTests
//
//  Created by Александр Евсеев on 15/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import XCTest
@testable import TestingApplication

class DataManagerTests: XCTestCase {

    var sut: DataManager?
    
    override func setUp() {
        super.setUp()
        
        sut = DataManager()
        sut?.dataSource = Dummy()
    }

    override func tearDown() {
        
        sut = nil
        super.tearDown()
    }
    
    func testDataManagerInitNormal() {
        XCTAssertNotNil(sut)
    }
    
    func testDataManagerSetDataSource() {
        
        XCTAssertNotNil(sut?.dataSource)
    }
    
    func testDataManagerFetchDataMethodReturnData() {
        
        sut?.fetchAndReadyData(completion: { (response) in
            XCTAssertNotNil(response)
        })
    }
    
    func testDataManagerSaveDataMethodDoSomething() {
        let sut1 = MockDataManager()
        let dummy = StructDataModel.init(data: [DataModel.init(nameOf: "Foo", priceOf: 10, countOf: 10)])
        sut1.saveDataInSource(dummy)
        
        XCTAssertTrue(sut1.methodFlag)
    }
}

extension DataManagerTests {
    class MockDataManager: DataManager {
        
        var methodFlag = false
        
        override func saveDataInSource(_ data: StructDataModel) {
            methodFlag = true
        }
    }
}
