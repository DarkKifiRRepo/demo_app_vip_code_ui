//
//  DataModelTests.swift
//  TestingApplicationTests
//
//  Created by Александр Евсеев on 15/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import XCTest
@testable import TestingApplication

class DataModelTests: XCTestCase {
    
    func testCanInitDataModelWithFullParams() {
        let sut = DataModel.init(nameOf: "Foo", priceOf: 10, countOf: 10)
        
        XCTAssertEqual(sut.name, "Foo")
        XCTAssertEqual(sut.price, 10)
        XCTAssertEqual(sut.count, 10)
    }
    
    func testCanInitDataModelWithoutSomeParams() {
        let sut = DataModel.init(nameOf: nil, priceOf: 10, countOf: 10)
        
        XCTAssertNil(sut.name)
        XCTAssertEqual(sut.price, 10)
        XCTAssertEqual(sut.count, 10)
    }
    
    func testCanInitDataModelWithoutParams() {
        let sut = DataModel()
        
        XCTAssertEqual(sut.name, "Dummy")
        XCTAssertEqual(sut.price, 0)
        XCTAssertEqual(sut.count, 0)
    }
    
    func testInitStructDataModelWithParam() {
        let dataModel = DataModel.init(nameOf: "Foo", priceOf: nil, countOf: nil)
        let sut = StructDataModel.init(data: [dataModel])
        
        XCTAssertEqual(sut.data.first?.name, "Foo")
    }
    
    func testInitStructDataModelWithEmptyArray() {
        let sut = StructDataModel.init(data: [])
        
        XCTAssertTrue(sut.data.isEmpty)
    }
    
    func testEquatableTwoDataModelValues() {
        let sut1 = DataModel.init(nameOf: "Foo", priceOf: 100, countOf: 10)
        let sut2 = DataModel.init(nameOf: "Foo", priceOf: 100, countOf: 10)
        let sut3 = DataModel.init(nameOf: "Bar", priceOf: 10, countOf: 10)
        
        XCTAssertTrue(sut1 == sut2)
        XCTAssertFalse(sut1 == sut3)
    }
}
