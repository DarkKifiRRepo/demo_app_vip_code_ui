//
//  CustomTableView.swift
//  TestingApplication
//
//  Created by Александр Евсеев on 10/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import UIKit

class CustomTableView: UITableView, viewStatusSettable {
    
    private var dataArray: [GoodsViewModel.Cell] = []
    
    init() {
        super.init(frame: CGRect.zero, style: .plain)
        
        translatesAutoresizingMaskIntoConstraints = false
        register(CustomTableViewCell.self, forCellReuseIdentifier: CustomTableViewCell.reuseId)
        
        delegate = self
        dataSource = self
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func refreshData(data: GoodsViewModel) {
        dataArray = data.cells
        reloadData()
    }
}

extension CustomTableView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? CustomTableViewCell else { return }
        guard let userData = cell.viewModel else { return }
        let dataToPass = (info: userData, position: indexPath.row)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "didSelectRowAt"),
                                        object: self,
                                        userInfo: ["cellData" : dataToPass])
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

extension CustomTableView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = dequeueReusableCell(withIdentifier: CustomTableViewCell.reuseId, for: indexPath) as! CustomTableViewCell
        let data = dataArray[indexPath.row]
        cell.setData(viewModel: data)
        return cell
    }
}
