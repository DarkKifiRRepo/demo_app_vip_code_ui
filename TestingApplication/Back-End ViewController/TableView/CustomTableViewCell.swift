//
//  CustomTableViewCell.swift
//  TestingApplication
//
//  Created by Александр Евсеев on 10/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import UIKit

protocol cellViewModel: class {
    var name: String? { get }
    var count: String? { get }
}

class CustomTableViewCell: UITableViewCell {

    static let reuseId = "CustomTableViewCell"
    var viewModel: GoodViewModel?
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Sizes.fontForLabel
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textColor = #colorLiteral(red: 0.1699317893, green: 0.1699317893, blue: 0.1699317893, alpha: 1)
        label.textAlignment = .left
        label.text = "Lenovo G580"
        return label
    }()
    
    let countLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Sizes.fontForLabel
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textColor = #colorLiteral(red: 0.2741315038, green: 0.2741315038, blue: 0.2741315038, alpha: 1)
        label.textAlignment = .right
        label.text = "1 шт."
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: CustomTableViewCell.reuseId)
        
        setupLabels()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        nameLabel.text = ""
        countLabel.text = ""
        selectionStyle = .none
    }
    
    func setData(viewModel: GoodViewModel) {
        nameLabel.text = viewModel.name
        countLabel.text = (viewModel.count ?? "0") + " шт."
        
        self.viewModel = viewModel
    }
    
    private func setupLabels() {
        
        addSubview(nameLabel)
        addSubview(countLabel)
        
        nameLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        nameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Sizes.dPadding).isActive = true
        nameLabel.widthAnchor.constraint(equalToConstant: Sizes.screenWidth * 0.8).isActive = true
        
        countLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        countLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Sizes.sPadding).isActive = true
        countLabel.widthAnchor.constraint(equalToConstant: Sizes.screenWidth * 0.2).isActive = true
    }
}
