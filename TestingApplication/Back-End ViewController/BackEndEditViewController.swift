//
//  BackEndEditViewController.swift
//  TestingApplication
//
//  Created by Александр Евсеев on 11/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation
import UIKit

class BackEndEditViewController: UIViewController {
    
    var dataForEdit: (info: GoodViewModel, position: Int?)?
    private var worker: BackEndWorker?
    weak var coordinator: TabBarController?
    
    @IBOutlet weak var nameTextLabel: UITextField!
    @IBOutlet weak var priceTextLabel: UITextField!
    @IBOutlet weak var countTextLabel: UITextField!
    @IBOutlet weak var countStepper: UIStepper!
    
    @IBAction func stepperPressed(_ sender: UIStepper) {
        countTextLabel.text = String(Int(countStepper.value))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.nameTextLabel.delegate = self
        setWorker(BackEndWorker(coordinator: coordinator))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let data = dataForEdit?.info {
            nameTextLabel.text = data.name
            priceTextLabel.text = data.price
            countStepper.value = (data.count! as NSString).doubleValue
            countTextLabel.text = data.count
        } else {
            nameTextLabel.text = "Dummy"
            priceTextLabel.text = "1000"
            countStepper.value = 10
            countTextLabel.text = "10"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel",
                                                           style: .plain,
                                                           target: self,
                                                           action: #selector(cancelPressed))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done",
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(donePressed))
    }
    
    @objc func cancelPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func donePressed() {
        saveNewModel()
        self.navigationController?.popViewController(animated: true)
    }
    
    func saveNewModel() {
        let result = GoodsViewModel.Cell.init(name: nameTextLabel.text ?? "Dummy",
                                              price: priceTextLabel.text ?? "1000",
                                              count: countTextLabel.text ?? "10")
        
        let data = dataForEdit?.info as? GoodsViewModel.Cell
        if result == data { return }
        
        worker?.updateObject(info: result, position: dataForEdit?.position)
    }
    
    func setWorker(_ worker: BackEndWorker?) {
        self.worker = worker
        self.coordinator = nil
    }
}

extension BackEndEditViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
