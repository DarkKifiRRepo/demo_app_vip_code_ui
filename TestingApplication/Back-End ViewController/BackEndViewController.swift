//
//  BackEndViewController.swift
//  TestingApplication
//
//  Created by Александр Евсеев on 09/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation
import UIKit

protocol ControllerNavigationDelegate: class {
    func presentEditViewController(DataToPass data: (GoodViewModel, Int?)?)
}

class BackEndViewController: UIViewController, viewStatusSettable {
    
    private var table: CustomTableView!
    var navigationDelegate: ControllerNavigationDelegate?
    weak var coordinator: TabBarController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(didSelectCell),
                                               name: NSNotification.Name("didSelectRowAt"),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(newDataRefresh),
                                               name: NSNotification.Name("NewData Arrieved"),
                                               object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setTableView()
        
        self.tabBarController?.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addButtonPressed))
        self.tabBarController?.navigationItem.title = "Back-End Tab"
        
        fetchDataOnAppear()
    }
    
    func setTableView(_ table: CustomTableView = CustomTableView() ) {
        self.table = table
        setViews()
    }
    
    private func setViews() {
        view.addSubview(table)
        table.fillSuperview()
        table.tableFooterView = UIView()
    }
    
    func refreshData(data: GoodsViewModel) {
        table.refreshData(data: data)
    }
    
    private func fetchDataOnAppear() {
        coordinator?.getRequest(request: CoordinatorRequestModel.Request.getAllData)
        guard let rawData = coordinator?.responseToRequest else { return }
        
        let viewModel = Adapters.modelToViewModel(model: rawData)
        refreshData(data: viewModel)
    }
    
    @objc func didSelectCell(notification: Notification) {
        guard
            let userInfo = notification.userInfo,
            let viewModel = userInfo["cellData"] as? (GoodViewModel, Int?) else { return }
        navigationDelegate?.presentEditViewController(DataToPass: viewModel)
    }
    
    @objc func addButtonPressed() {
        navigationDelegate?.presentEditViewController(DataToPass: nil)
    }
    
    @objc func newDataRefresh(notification: Notification) {
        guard let userInfo = notification.userInfo,
            let response = userInfo["NewData"] as? StructDataModel
            else { return }
        let dataViewModel = Adapters.modelToViewModel(model: response)
        refreshData(data: dataViewModel)
    }
}
