//
//  BackEndWorker.swift
//  TestingApplication
//
//  Created by Александр Евсеев on 14/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation

class BackEndWorker {
    
    private var coordinator: TabBarController?
    
    init(coordinator: TabBarController? = nil) {
        self.coordinator = coordinator
    }
    
    func updateObject(info: GoodViewModel, position: Int?) {
        let dataToPass = Adapters.viewModelToModel(viewModel: info)
        coordinator?.getRequest(request: CoordinatorRequestModel.Request.editGoodAt(position: position, good: dataToPass))
    }
}
