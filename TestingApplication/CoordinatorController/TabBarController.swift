//
//  TabBarController.swift
//  TestingApplication
//
//  Created by Александр Евсеев on 09/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import UIKit

protocol viewStatusSettable {
    func refreshData(data: GoodsViewModel)
}

protocol CoordinatorRequestLogic {
    func getRequest(request: CoordinatorRequestModel.Request)
}

class TabBarController: UITabBarController {
    
    var dataStore: CoordinatorDataStore?
    var responseToRequest: StructDataModel? {
        didSet {
            guard let response = responseToRequest, oldValue != response else { return }
            print("TabBarController store modified")
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: NSNotification.Name("NewData Arrieved"),
                                                object: nil,
                                                userInfo: ["NewData" : response])
            }
        }
    }
    var tabs = CoordinatorTabs()
    
    init(dataStore: CoordinatorDataStore? = CoordinatorDataStore() ) {
        super.init(nibName: nil, bundle: nil)
        self.dataStore = dataStore
        print("TabBarController initialize")
        
        tabs.storeFront?.interactor?.worker?.coordinator = self
        tabs.backEnd?.coordinator = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let storeFrontVC = StoreFrontViewController(transitionStyle: .scroll, navigationOrientation: .horizontal)
        storeFrontVC.tabBarItem = UITabBarItem(tabBarSystemItem: .bookmarks, tag: 0)
        tabs.storeFront = storeFrontVC
        viewControllers = [storeFrontVC]
        
        if let backEndVC = UIStoryboard.init(name: "BackEndFlow", bundle: nil).instantiateInitialViewController() {
            (backEndVC  as? BackEndViewController)?.navigationDelegate = self
            backEndVC.tabBarItem = UITabBarItem(tabBarSystemItem: .featured, tag: 1)
            tabs.backEnd = backEndVC as? BackEndViewController
            viewControllers?.append(backEndVC)
        }
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(newDataRefresh),
                                               name: NSNotification.Name("Coordinator Have New Data"),
                                               object: nil)
    }
    
    @objc func newDataRefresh() {
        dataStore?.getDataFromStore(handler: { [weak self] (newData) in
            if self?.responseToRequest == newData { return }
            self?.responseToRequest = newData
        })
    }
}

extension TabBarController: CoordinatorRequestLogic {
    func getRequest(request: CoordinatorRequestModel.Request) {
        switch request {
        case .getAllData:
            dataStore?.getDataFromStore(handler: { (response) in
                self.responseToRequest = response
            })
        case .editGoodAt(let position, let good):
            dataStore?.editGood(info: (data: good, position: position))
        case .deleteGoodAt(let position):
            print("Dummy .deleteGoodAt ", position)
        case .buyGood(let good):
            dataStore?.buyGood(info: good)
        }
    }
}

extension TabBarController: ControllerNavigationDelegate {
    func presentEditViewController(DataToPass data: (GoodViewModel, Int?)?) {
        guard let editVC = UIStoryboard.init(name: "BackEndFlow", bundle: nil).instantiateViewController(withIdentifier: String(describing: BackEndEditViewController.self)) as? BackEndEditViewController else { return }
        editVC.dataForEdit = data
        editVC.coordinator = self
        navigationController?.pushViewController(editVC, animated: true)
    }
}
