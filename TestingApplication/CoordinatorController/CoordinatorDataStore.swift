//
//  CoordinatorDataStore.swift
//  TestingApplication
//
//  Created by Александр Евсеев on 12/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation

class CoordinatorDataStore {
    
    var store: StructDataModel?
    var dataManager: DataManager?
    private let queue = DispatchQueue(label: "Second.Processing")
    
    init(dataManager: DataManager? = DataManager() ) {
        self.dataManager = dataManager
        print("Coordinator DataStore initialize")
        getFetch()
    }
    
    private func storeChangeValue() {
        guard let store = self.store else { return }
        dataManager?.saveDataInSource(store)
        NotificationCenter.default.post(name: NSNotification.Name("Coordinator Have New Data"), object: nil)
    }
    
    private func queueDispatcher(handler: @escaping () -> Void, notify: @escaping () -> Void ) {
        let workItem = DispatchWorkItem.init {
            handler()
        }
        queue.async(execute: workItem)
        workItem.notify(queue: .main) {
            notify()
        }
    }
    
    private func getFetch() {
        dataManager?.fetchAndReadyData { (response) in
            self.store = response
        }
    }
    
    private func buyGoodInner(info: DataModel) -> Bool {
//        print(#function, " called")
        sleep(3) // Покупка продукции занимает длительное время: 3 секунды
//        print(#function, " start")
        guard store != nil,
            let index = store?.data.firstIndex(of: info),
            (store?.data[index].count)! >= 1,
            let dataOnIndex = store?.data[index]
            else { return false }
        guard dataOnIndex.count != nil else { return false }
        let product = DataModel.init(nameOf: dataOnIndex.name, priceOf: dataOnIndex.price, countOf: dataOnIndex.count! - 1 )
        editGoodInner(info: (data: product, position: index))
        return true
    }
    
    private func editGoodInner(info: (data: DataModel, position: Int?) ) {
//        print(#function, " called")
        sleep(5) // Сохранение данных после редактирования занимает длительное время: 5 секунды
//        print(#function, " start")
        if store == nil { store = StructDataModel.init(data: []) }
        if info.position == nil {
            store?.data.append(info.data)
            print("Coordinator DataStore - Append new element")
            storeChangeValue()
        } else {
            store?.data[info.position!] = info.data
            print("Coordinator DataStore - Modifed element")
            storeChangeValue()
        }
    }
    
    // MARK: - public function
    
    func getDataFromStore(handler: @escaping (StructDataModel) -> Void ) {
        guard let store = self.store else { return }
        handler( store )
    }
    
    func editGood(info: (data: DataModel, position: Int?) ) {
        queueDispatcher(handler: {
            self.editGoodInner(info: info)
        }, notify: {})
    }
    
    func buyGood(info: DataModel) {
        var result: Bool?
        queueDispatcher(handler: {
            result = self.buyGoodInner(info: info)
        }) {
            if result ?? false { print("Successful purchase") } else { print("Unsuccessful purchase") }
        }
    }
}
