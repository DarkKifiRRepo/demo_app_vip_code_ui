//
//  CoordinatorRequestModel.swift
//  TestingApplication
//
//  Created by Александр Евсеев on 12/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation

struct CoordinatorRequestModel {
    
    enum Request {
        case getAllData
        case editGoodAt(position: Int?, good: DataModel)
        case deleteGoodAt(position: Int)
        case buyGood(good: DataModel)
    }
}

struct CoordinatorTabs {
    var storeFront: StoreFrontViewController?
    var backEnd: BackEndViewController?
}
