//
//  StoreFrontViewController.swift
//  TestingApplication
//
//  Created by Александр Евсеев on 09/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation
import UIKit

protocol DisplayLogic: class {
    func displayData(viewModel: DataTransfer.Display.ViewModel)
}

class StoreFrontViewController: UIPageViewController, DisplayLogic {
    
    var interactor: BusinessLogic?
    private var viewsForController: [GalleryViewController] = []
    private var currentPage: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        setupMainView()
        
        view.backgroundColor = .white
        
        setViewControllers([ BlankViewController() ], direction: .forward, animated: false, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.navigationItem.title = "Store-Front Tab"
        self.tabBarController?.navigationItem.rightBarButtonItem = nil
        
        interactor?.getRequest(request: DataTransfer.Request.RequestType.updatePresentData)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        viewsForController = []
        super.viewDidDisappear(animated)
    }
    
    private func setup() {
        let viewController          = self
        let interactor              = StoreFrontInterator()
        let presenter               = StoreFrontPresenter()
        let worker                  = StoreFrontWorker(coordinator: nil)
        viewController.interactor   = interactor
        interactor.presenter        = presenter
        interactor.worker           = worker
        presenter.viewController    = viewController
    }
    
    func displayData(viewModel: DataTransfer.Display.ViewModel) {
        switch viewModel {
        case .displayInitData(let viewModel):
            checkViewModel(viewModel)
        case .displayBuyAction(let viewModel):
            checkViewModel(viewModel)
        }
    }
    
    private func setupMainView() {
        delegate = self
        dataSource = self
    }
    
    private func checkViewModel(_ model: viewModelVC) {
        if !(model.viewsArray.isEmpty) {
            viewsForController = model.viewsArray
            updatePageSource()
        } else {
            currentPage = 0
            setViewControllers([ BlankViewController() ], direction: .forward, animated: true, completion: nil)
        }
    }
    
    private func updatePageSource() {
        let pageCount = viewsForController.count - 1
        if currentPage > pageCount { currentPage = pageCount }
        
        if !(viewsForController.isEmpty) {
            setViewControllers([ viewsForController[currentPage] ], direction: .forward, animated: false, completion: nil)
        }
    }
}

extension StoreFrontViewController: ButtonPressDelegate {
    func buttonPressed(userInfo: GoodViewModel) {
        interactor?.getRequest(request: DataTransfer.Request.RequestType.buyButtonPressed(userInfo: userInfo))
    }
}

extension StoreFrontViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard completed, let pageTag = pageViewController.viewControllers?.first?.view.tag else { return }
        self.currentPage = pageTag
    }
}

extension StoreFrontViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let vcIndex = viewsForController.firstIndex(of: viewController as! GalleryViewController) else { return nil }
        let previousIndex = vcIndex - 1
        guard previousIndex >= 0 else { return nil }
        guard viewsForController.count > previousIndex else { return nil }
        return viewsForController[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let vcIndex = viewsForController.firstIndex(of: viewController as! GalleryViewController) else { return nil }
        let nextIndex = vcIndex + 1
        guard viewsForController.count != nextIndex else { return nil }
        guard viewsForController.count > nextIndex else { return nil }
        return viewsForController[nextIndex]
    }
    
    // For PageControl uncomment two function after
    
//    func presentationCount(for pageViewController: UIPageViewController) -> Int {
//        return viewsForController.count
//    }
//
//    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
//        guard let firstVC = viewControllers?.first,
//            let firstVCIndex = viewsForController.firstIndex(of: firstVC as! GalleryViewController)
//            else { return 0 }
//        return firstVCIndex
//    }
}
