//
//  StoreFrontWorker.swift
//  TestingApplication
//
//  Created by Александр Евсеев on 10/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation


class StoreFrontWorker {
    
    weak var coordinator: TabBarController?
    
    init(coordinator: TabBarController?) {
        print("StoreFrontWorker initialize")
        self.coordinator = coordinator

    }
    
    func takeData(completion: @escaping (StructDataModel) -> Void) {
        coordinator?.getRequest(request: CoordinatorRequestModel.Request.getAllData)
        
        guard let response = coordinator?.responseToRequest else { return }
        completion(response)
    }
    
    func buyGood(userInfo: GoodViewModel, completion: @escaping (StructDataModel) -> Void) {
        let model = Adapters.viewModelToModel(viewModel: userInfo)
        coordinator?.getRequest(request: CoordinatorRequestModel.Request.buyGood(good: model ))
        
        guard let response = coordinator?.responseToRequest else { return }
        completion(response)
    }
}
