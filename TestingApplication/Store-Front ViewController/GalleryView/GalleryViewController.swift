//
//  GalleryViewController.swift
//  TestingApplication
//
//  Created by Александр Евсеев on 10/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import UIKit

protocol ButtonPressDelegate: class {
    func buttonPressed(userInfo: GoodViewModel)
}

class GalleryViewController: UIViewController {
    
    weak var delegate: ButtonPressDelegate?
    var viewModel: GoodViewModel?
    
    //MARK: Implementaion first layer
    let nameView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        return view
    }()
    
    let priceView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        return view
    }()
    
    let countView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        return view
    }()
    
    let buyView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        return view
    }()
    
    //MARK: Implementaion second layer
    let nameViewLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Sizes.fontForLabel
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textColor = #colorLiteral(red: 0.1699317893, green: 0.1699317893, blue: 0.1699317893, alpha: 1)
        label.textAlignment = .center
        label.text = "Lenovo G580"
        return label
    }()
    
    let priceViewSimpleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Sizes.fontForLabel
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textColor = #colorLiteral(red: 0.1699317893, green: 0.1699317893, blue: 0.1699317893, alpha: 1)
        label.textAlignment = .center
        label.text = "Цена"
        return label
    }()
    
    let priceViewPriceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Sizes.fontForLabel
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textColor = #colorLiteral(red: 0.2741315038, green: 0.2741315038, blue: 0.2741315038, alpha: 1)
        label.textAlignment = .center
        label.text = "8 922 руб."
        return label
    }()
    
    let countViewSimpleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Sizes.fontForLabel
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textColor = #colorLiteral(red: 0.1699317893, green: 0.1699317893, blue: 0.1699317893, alpha: 1)
        label.textAlignment = .center
        label.text = "Количество"
        return label
    }()
    
    let countViewCountLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Sizes.fontForLabel
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textColor = #colorLiteral(red: 0.2741315038, green: 0.2741315038, blue: 0.2741315038, alpha: 1)
        label.textAlignment = .center
        label.text = "1 шт."
        return label
    }()
    
    let buyButton: UIButton = {
        let button = UIButton()
        button.setTitle("Купить", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = Sizes.fontForButton
        button.setTitleColor(#colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1), for: .normal)
        button.contentEdgeInsets = Sizes.insetForButton
        button.contentHorizontalAlignment = .center
        button.contentVerticalAlignment = .center
        button.layer.borderWidth = 1
        button.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        button.layer.cornerRadius = 8
        button.clipsToBounds = true
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        
        setupFirstLayer()
        setupNameView()
        setupPriceView()
        setupCountView()
        setupBuyView()
        
        buyButton.addTarget(self, action: #selector(buyButtonPressed), for: .touchUpInside)
    }
    
    //MARK: Initialization
    
    init() {
        super.init(nibName: String(describing: GalleryViewController.self), bundle: nil)
    }
    
    convenience init(with viewModel: GoodViewModel, delegate: DisplayLogic? = nil, tag: Int = 0) {
        self.init()
        setData(with: viewModel)
        self.delegate = delegate as? ButtonPressDelegate
        self.view.tag = tag
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: Functonaluty
    
    @objc func buyButtonPressed() {
        guard let viewModel = viewModel else { return }
        delegate?.buttonPressed(userInfo: viewModel)
    }
    
    func updateViewModel(with newViewModel: GoodViewModel) {
        setData(with: newViewModel)
    }
    
    func setData(with viewModel: GoodViewModel) {
        nameViewLabel.text = viewModel.name
        priceViewPriceLabel.text = (viewModel.price ?? "0") + " руб."
        countViewCountLabel.text = (viewModel.count ?? "0") + " шт."
        self.viewModel = viewModel
    }
    
    //MARK: Setups Layers
    
    private func setupFirstLayer() {
        view.addSubview(nameView)
        view.addSubview(priceView)
        view.addSubview(countView)
        view.addSubview(buyView)
        
        nameView.anchor(top: view.topAnchor,
                        leading: view.leadingAnchor,
                        bottom: nil,
                        trailing: view.trailingAnchor,
                        padding: UIEdgeInsets(top: Sizes.topViewInset + Sizes.sPadding, left: 1, bottom: 777, right: 1),
                        size: CGSize(width: 0, height: Sizes.screenHeight / 4))
        priceView.anchor(top: nameView.bottomAnchor,
                         leading: view.leadingAnchor,
                         bottom: nil,
                         trailing: view.trailingAnchor,
                         padding: UIEdgeInsets(top: Sizes.sPadding, left: 1, bottom: 777, right: 1),
                         size: CGSize(width: 0, height: Sizes.screenHeight / 4))
        countView.anchor(top: priceView.bottomAnchor,
                         leading: view.leadingAnchor,
                         bottom: nil,
                         trailing: view.trailingAnchor,
                         padding: UIEdgeInsets(top: Sizes.sPadding, left: 1, bottom: 777, right: 1),
                         size: CGSize(width: 0, height: Sizes.screenHeight / 4))
        buyView.anchor(top: countView.bottomAnchor,
                         leading: view.leadingAnchor,
                         bottom: nil,
                         trailing: view.trailingAnchor,
                         padding: UIEdgeInsets(top: Sizes.sPadding, left: 1, bottom: 777, right: 1),
                         size: CGSize(width: 0, height: Sizes.screenHeight / 4))
    }
    
    private func setupNameView() {
        nameView.addSubview(nameViewLabel)
        
        nameViewLabel.centerXAnchor.constraint(equalTo: nameView.centerXAnchor).isActive = true
        nameViewLabel.centerYAnchor.constraint(equalTo: nameView.centerYAnchor).isActive = true
    }
    
    private func setupPriceView() {
        priceView.addSubview(priceViewSimpleLabel)
        priceView.addSubview(priceViewPriceLabel)
        
        priceViewSimpleLabel.centerYAnchor.constraint(equalTo: priceView.centerYAnchor).isActive = true
        priceViewSimpleLabel.leadingAnchor.constraint(equalTo: priceView.leadingAnchor, constant: Sizes.dPadding).isActive = true
        
        priceViewPriceLabel.centerYAnchor.constraint(equalTo: priceView.centerYAnchor).isActive = true
        priceViewPriceLabel.trailingAnchor.constraint(equalTo: priceView.trailingAnchor, constant: -Sizes.dPadding).isActive = true
    }
    
    private func setupCountView() {
        countView.addSubview(countViewSimpleLabel)
        countView.addSubview(countViewCountLabel)
        
        countViewSimpleLabel.centerYAnchor.constraint(equalTo: countView.centerYAnchor).isActive = true
        countViewSimpleLabel.leadingAnchor.constraint(equalTo: countView.leadingAnchor, constant: Sizes.dPadding).isActive = true
        
        countViewCountLabel.centerYAnchor.constraint(equalTo: countView.centerYAnchor).isActive = true
        countViewCountLabel.trailingAnchor.constraint(equalTo: countView.trailingAnchor, constant: -Sizes.dPadding).isActive = true
    }
    
    private func setupBuyView() {
        buyView.addSubview(buyButton)
        
        buyButton.centerXAnchor.constraint(equalTo: buyView.centerXAnchor).isActive = true
        buyButton.centerYAnchor.constraint(equalTo: buyView.centerYAnchor).isActive = true
    }
}
