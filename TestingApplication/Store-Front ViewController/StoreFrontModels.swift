//
//  StoreFrontModels.swift
//  TestingApplication
//
//  Created by Александр Евсеев on 09/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation

enum DataTransfer {
    
    struct Request {
        enum RequestType {
            case updatePresentData
            case buyButtonPressed(userInfo: GoodViewModel)
        }
    }
    
    struct Present {
        enum PresentType {
            case presentInitFetch(data: StructDataModel)
            case presentBuyAction(data: StructDataModel)
        }
    }
    
    struct Display {
        enum ViewModel {
            case displayInitData(viewModel: viewModelVC)
            case displayBuyAction(viewModel: viewModelVC)
        }
    }
}

protocol GoodViewModel {
    var name: String? { get }
    var price: String? { get }
    var count: String? { get }
}

struct GoodsViewModel {
    struct Cell: GoodViewModel, Equatable {
        var name: String?
        var price: String?
        var count: String?
    }
    
    let cells: [Cell]
}

struct viewModelVC {
    let viewsArray: [GalleryViewController]
}


