//
//  StoreFrontPresenter.swift
//  TestingApplication
//
//  Created by Александр Евсеев on 09/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation

protocol Presentable {
    func presentData(response: DataTransfer.Present.PresentType)
}

class StoreFrontPresenter: Presentable {
    
    var viewController: DisplayLogic?
    
    func presentData(response: DataTransfer.Present.PresentType) {
        switch response {
        case .presentInitFetch(let data):
            let viewModel = readyDataForViewModel(response: data)
            viewController?.displayData(viewModel: DataTransfer.Display.ViewModel.displayInitData(viewModel: viewModel))
        case .presentBuyAction(let data):
            let viewModel = readyDataForViewModel(response: data)
            viewController?.displayData(viewModel: DataTransfer.Display.ViewModel.displayBuyAction(viewModel: viewModel))
        }
    }
    
    private func readyDataForViewModel(response: StructDataModel) -> viewModelVC {
        let viewsVC = response.data.map { model in
            GalleryViewController.init(with: Adapters.modelToViewModel(model: model), delegate: viewController!)
        }
        
        let readyVCs = viewsVC.filter { (view) -> Bool in // remove views with zero good count
            !(view.viewModel?.count == "0") ? true : false
        }
        
        for tuple in readyVCs.enumerated() { // set tag for each viewController. It's for track paging
            tuple.element.view.tag = tuple.offset
        }
        
        let result = viewModelVC.init(viewsArray: readyVCs)
        return result
    }
}
