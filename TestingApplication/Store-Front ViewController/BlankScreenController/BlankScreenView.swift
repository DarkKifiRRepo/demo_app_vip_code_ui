//
//  BlankViewController.swift
//  TestingApplication
//
//  Created by Александр Евсеев on 28/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import UIKit

class BlankViewController: UIViewController {

    let blankText: UILabel = {
        let label = UILabel()
        label.textColor = #colorLiteral(red: 0.3276491117, green: 0.3276491117, blue: 0.3276491117, alpha: 1)
        label.font = UIFont.systemFont(ofSize: 28)
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 2
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Shop don't have any goods, sorry."
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
    }
    
    private func setupViews() {
        view.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        view.addSubview(blankText)
        
        blankText.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        blankText.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Sizes.dPadding).isActive = true
        blankText.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Sizes.dPadding).isActive = true
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
