//
//  StoreFrontInterator.swift
//  TestingApplication
//
//  Created by Александр Евсеев on 09/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation

protocol BusinessLogic {
    var worker: StoreFrontWorker? { get }
    func getRequest(request: DataTransfer.Request.RequestType)
}

class StoreFrontInterator: BusinessLogic {
    
    var presenter: Presentable?
    var worker: StoreFrontWorker?
    
    init() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(newDataRefresh),
                                               name: NSNotification.Name("NewData Arrieved"),
                                               object: nil)
    }
    
    func getRequest(request: DataTransfer.Request.RequestType) {
        switch request {
        case .updatePresentData:
//            print(".updatePresentData interator")
            worker?.takeData { [weak self] (data) in
                self?.presenter?.presentData(response: DataTransfer.Present.PresentType.presentInitFetch(data: data))
            }
        case .buyButtonPressed(let userInfo):
            print(".buyButtonPressed with sender: ", userInfo)
            worker?.buyGood(userInfo: userInfo) { [weak self] (response) in
                self?.presenter?.presentData(response: DataTransfer.Present.PresentType.presentBuyAction(data: response))
            }
        }
    }
    
    @objc func newDataRefresh(notification: Notification) {
        guard let userInfo = notification.userInfo,
            let response = userInfo["NewData"] as? StructDataModel
            else { return }
        self.presenter?.presentData(response: DataTransfer.Present.PresentType.presentInitFetch(data: response))
    }
}
