//
//  DataModel.swift
//  TestingApplication
//
//  Created by Александр Евсеев on 09/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation

struct StructDataModel: Equatable, Codable {
    var data: [DataModel]
    
    static func == (lhs: StructDataModel, rhs: StructDataModel) -> Bool {
        for index in lhs.data.enumerated() {
            if index.element.name != rhs.data[index.offset].name ||
                index.element.price != rhs.data[index.offset].price ||
                index.element.count != rhs.data[index.offset].count { return false }
        }
        return true
    }
}

struct DataModel: Equatable, Codable {
    var name: String?
    var price: Double?
    var count: Int?
    
    init(nameOf name: String? = "Dummy", priceOf price: Double? = 0, countOf count: Int? = 0) {
        self.name = name
        self.price = price
        self.count = count
    }
    
    static func == (lhs: DataModel, rhs: DataModel) -> Bool {
        if lhs.name == rhs.name, lhs.price == rhs.price { return true } else { return false }
    }
}
