//
//  DummyData.swift
//  TestingApplication
//
//  Created by Александр Евсеев on 10/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation

class Dummy: DataManagable, Equatable {
    
    func fetchData() -> StructDataModel? {
        let result = StructDataModel.init(data: dummyArray)
        return result
    }
    
    func saveData(dataToSave data: StructDataModel) {
        print("Dummy save data")
    }
    
    private var dummyArray: [DataModel] = [DataModel(nameOf: "Apple iPod touch 5 32Gb", priceOf: 8888, countOf: 5),
                                           DataModel(nameOf: "Samsung Galaxy S Duos S7562", priceOf: 7230, countOf: 2),
                                           DataModel(nameOf: "Canon EOS 600D Kit", priceOf: 15659, countOf: 4),
                                           DataModel(nameOf: "Samsung Galaxy Tab 2 10.1 P5100 16Gb", priceOf: 13290, countOf: 9),
                                           DataModel(nameOf: "PocketBook Touch", priceOf: 5197, countOf: 2),
                                           DataModel(nameOf: "Samsung Galaxy Note II 16Gb", priceOf: 17049.50, countOf: 2),
                                           DataModel(nameOf: "Nikon D3100 Kit", priceOf: 12190, countOf: 4),
                                           DataModel(nameOf: "Canon EOS 1100D Kit", priceOf: 10985, countOf: 2),
                                           DataModel(nameOf: "Sony Xperia acro S", priceOf: 11800.99, countOf: 1),
                                           DataModel(nameOf: "Lenovo G580", priceOf: 8922, countOf: 1)]
    
    static func == (lhs: Dummy, rhs: Dummy) -> Bool {
        if lhs.dummyArray.first?.name == rhs.dummyArray.first?.name &&
            lhs.dummyArray.first?.price == rhs.dummyArray.first?.price {
            return true
        }
        return false
    }
}
