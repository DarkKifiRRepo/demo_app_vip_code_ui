//
//  JSONGrabber.swift
//  TestingApplication
//
//  Created by Александр Евсеев on 16/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation

class JSONGrabber: DataManagable {
    
    private let fileSource = "Foo"
    private let fileType = "json"
    
    func fetchData() -> StructDataModel? {
        guard let fetchedData = readFile() else { return nil }
        let decodedArray = decodeJSONData(encodedData: fetchedData)
        return decodedArray
    }
    
    func saveData(dataToSave data: StructDataModel) {
        guard let dataToSave = encodeJSONData(decodedArray: data) else { return }
        reWriteFile(encodingArray: dataToSave)
    }
    
    private func decodeJSONData(encodedData: Data) -> StructDataModel? {
        let decoder = JSONDecoder.init()
        let decodedData = try? decoder.decode(StructDataModel.self, from: encodedData)
        return decodedData
    }
    
    private func encodeJSONData(decodedArray: StructDataModel) -> Data? {
        let encoder = JSONEncoder.init()
        let encodedData = try? encoder.encode(decodedArray)
        return encodedData
    }
    
    // MARK: - file work methods
    
    private func takeFileURL (FullFileName destFileName: String) -> URL? {
        guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last
            else {
                print("Take File URL: No document directory found in application bundle.")
                return nil
        }
        let writableFileURL = documentsDirectory.appendingPathComponent(destFileName)
        
        return writableFileURL
    }
    
    private func readFile () -> Data? {
        var fileData = Data ()
        guard let url = takeFileURL(FullFileName: fileSource + "." + fileType) else { return nil }
        do {
            fileData = try Data(contentsOf: url)
        } catch { print("Catch Read File ERROR: \(error)") }
        
//        print("Read file - OK: Data: \(fileData) from file: \(fileSource + "." + fileType)")
        
        return fileData
    }
    
    private func reWriteFile (encodingArray array: Data) {
        guard let url = takeFileURL(FullFileName: fileSource + "." + fileType) else { return }
        do {
            try array.write(to: url)
        } catch { print("Catch ReWrite Error ERROR: \(error.localizedDescription)") }
        
//        print("File rewrite - OK: to file: \(fileSource + "." + fileType)")
    }
}
