//
//  DataManager.swift
//  TestingApplication
//
//  Created by Александр Евсеев on 10/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation

protocol DataManagable: class {
    func fetchData() -> StructDataModel?
    func saveData(dataToSave data: StructDataModel)
}

class DataManager {
    
    var dataSource: DataManagable?
    
    func fetchAndReadyData(completion: @escaping (StructDataModel) -> Void) {
        if let array = dataSource?.fetchData() {
            completion(array)
        }
    }
    
    func saveDataInSource(_ data: StructDataModel) {
        dataSource?.saveData(dataToSave: data)
    }
    
    init() {
        
        // There we can change data source to other type .json, .csv, .xml, and other
        // New module (data parcer) must be sign to DataManagable protocol
        // For Dummy Data uncomment next line and comment JSONGrabber
        
//        self.dataSource = Dummy()
        self.dataSource = JSONGrabber()
        print("DataManager Initialize")
    }
}
