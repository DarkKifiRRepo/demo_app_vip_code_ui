//
//  AppDelegate.swift
//  TestingApplication
//
//  Created by Александр Евсеев on 09/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var tabBar: UITabBar?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = UINavigationController(rootViewController: TabBarController(dataStore: CoordinatorDataStore()))
        window?.makeKeyAndVisible()
        
        return true
    }
}

