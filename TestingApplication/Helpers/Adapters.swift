//
//  Adapters.swift
//  TestingApplication
//
//  Created by Александр Евсеев on 11/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation

class Adapters {
    
    static func modelToViewModel(model: DataModel) -> GoodViewModel {
        var stringPrice = "0"
        var stringCount = "0"
        if let intPrice = model.price { stringPrice = String(intPrice) }
        if let intCount = model.count { stringCount = String(intCount) }
        let result = GoodsViewModel.Cell.init(name: model.name, price: stringPrice, count: stringCount)
        return result
    }
    
    static func modelToViewModel(model: StructDataModel) -> GoodsViewModel {
        let arrayOfCells = model.data.map { dataModel in
            GoodsViewModel.Cell.init(name: Adapters.modelToViewModel(model: dataModel).name,
                                     price: Adapters.modelToViewModel(model: dataModel).price,
                                     count: Adapters.modelToViewModel(model: dataModel).count)
        }
        let result = GoodsViewModel.init(cells: arrayOfCells)
        return result
    }
    
    static func viewModelToModel(viewModel: GoodViewModel) -> DataModel {
        let name = viewModel.name
        let price = (viewModel.price as NSString?)?.doubleValue ?? 0
        let count = (viewModel.count as NSString?)?.integerValue ?? 0
        let result = DataModel.init(nameOf: name, priceOf: price, countOf: count)
        return result
    }
}
