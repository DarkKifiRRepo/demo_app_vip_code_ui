//
//  Sizes.swift
//  TestingApplication
//
//  Created by Александр Евсеев on 10/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation
import UIKit

struct Sizes {
    static let screenWidth = UIScreen.main.bounds.width
    static let navContrHeight = UINavigationController().navigationBar.frame.height
    static let tabBarHeight = UITabBarController().tabBar.frame.height
    static let screenHeight = UIScreen.main.bounds.height - topViewInset - tabBarHeight - dPadding * 2.5
    static let sPadding: CGFloat = 4
    static let dPadding: CGFloat = sPadding * 2
    static let topViewInset: CGFloat = UIApplication.shared.statusBarFrame.height + navContrHeight
    static let fontForLabel: UIFont = UIFont.systemFont(ofSize: 20)
    static let fontForButton: UIFont = UIFont.systemFont(ofSize: 32, weight: .semibold)
    static let insetForButton = UIEdgeInsets(top: sPadding, left: dPadding * 2, bottom: sPadding, right: dPadding * 2)
}
